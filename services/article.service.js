const express = require('express')

const articles = [
    {
        id: 123452,
        name: 'Toothbrush',
        price: '€ 126.00',
        active: true
    },
    {
        id: 821934,
        name: 'Chair',
        price: '€ 124.87',
        active: false
    }
];

module.exports = {
    getArticles: (req, res) => {
        return res.status(200).json(articles)
    }
}
