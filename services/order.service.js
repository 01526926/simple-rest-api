const express = require('express')

const orders = [
  {
    id: 1,
    articleId: 77857,
    quantity: 5,
    completed: true
  },
  {
    id: 2,
    articleId: 55,
    quantity: 56,
    completed: false
  }
];

module.exports = {
    getOrders: (req, res) => {
        return res.status(200).json(orders)
    }
}
