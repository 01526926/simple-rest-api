const express = require('express')
const router = express.Router()

const orderService = require('../services/order.service')

router.get('', orderService.getOrders)

module.exports = router
