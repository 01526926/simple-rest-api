const express = require('express')
const router = express.Router()

const articleService = require('../services/article.service')

router.get('', articleService.getArticles)

module.exports = router
