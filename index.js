const express = require('express')
var cors = require('cors')

const menuEndpoint = require('./endpoints/article.endpoint')
const orderEndpoint = require('./endpoints/order.endpoint')

const app = express()

app.use(cors())
app.use(express.json())


app.use('/article', menuEndpoint)
app.use('/order', orderEndpoint)

module.exports = app
